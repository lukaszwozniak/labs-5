﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PK.Container;


namespace Kontener
{
    public class Kontener: IContainer
    {

        private Dictionary<Type, Type> _kontenerTypow;
        private Dictionary<Type, object> _kontenerObiektow;
        // Tutaj przetrzymujemy funkcje, ktore tworza nam odpowiednie typy
        private Dictionary<Type, object> _kontenerFunkcji;

        // Juz nie singleton, bo ciezej byloby zrobic zeby przechodzil testy (wielokrotne rejestrowanie tych samych typow w ramach wszystkich funkcji testowych)
        public Kontener()
        {
            _kontenerTypow = new Dictionary<Type, Type>();
            _kontenerObiektow = new Dictionary<Type, object>();
            _kontenerFunkcji = new Dictionary<Type, object>();
        }

        public void Register(System.Reflection.Assembly assembly)
        {
            foreach (var type in assembly.GetTypes())
            {
                if (type.IsInterface || !type.IsPublic)
                    continue;

                Register(type);
            }
        }

        public void Register(Type type)
        {
            foreach (var interfejs in type.GetInterfaces())
            {
                if (!_kontenerTypow.ContainsKey(interfejs))
                {
                    _kontenerTypow.Add(interfejs, type);
                }                
            }
        }

        public void Register<T>(T impl) where T : class
        {
            Type type = impl.GetType();

            foreach (var interfejs in type.GetInterfaces())
            {
                if (_kontenerTypow.ContainsKey(interfejs))
                {
                    continue;
                }

                _kontenerObiektow.Add(interfejs, impl);
            }
        }

        public void Register<T>(Func<T> provider) where T : class
        {
            _kontenerFunkcji.Add(typeof(T), provider);
            Register(typeof(T));
        }

        public T Resolve<T>() where T : class
        {
            Type t = typeof(T);

            if (_kontenerObiektow.ContainsKey(t))
            {
                return (T)_kontenerObiektow[t];
            }

            if (_kontenerFunkcji.ContainsKey(t))
                return ((Func<T>)_kontenerFunkcji[t])();


            if (_kontenerTypow.ContainsKey(t))
            {
                var resolvedType = _kontenerTypow[t];

                if (_kontenerFunkcji.ContainsKey(resolvedType))
                    return ((Func<T>)_kontenerFunkcji[resolvedType])();

                return (T)Resolve(t);
            }

            return null;
        }

        public object Resolve(Type type)
        {
            if (!_kontenerTypow.ContainsKey(type))
            {
                return null;              
            }

            var resolvedType = _kontenerTypow[type];            

            if (resolvedType.GetConstructor(Type.EmptyTypes) != null)
            {
                return Activator.CreateInstance(resolvedType);
            }
            else
            {
                var parameters = ResolveParameters(resolvedType);

                if (parameters == null)
                {
                    throw new UnresolvedDependenciesException(String.Format("Nie mozna rozwiazac zaleznosci dla {0}", resolvedType));     
                }

                return Activator.CreateInstance(resolvedType, parameters);
            }            
        }

        private object[] ResolveParameters(Type type)
        {
            var cInfos = type.GetConstructors();

            // Znajdujemy pierwszy lepszy konstruktor, dla ktorego umiemy rozwiazac zaleznosci
            foreach (var cInfo in cInfos)
            {
                bool stop = false;

                var pInfos = cInfo.GetParameters();
                foreach (var pInfo in pInfos)
                {
                    if (!_kontenerTypow.ContainsKey(pInfo.ParameterType))
                    {
                        stop = true;
                        break;
                    }
                }

                if (stop)
                {
                    continue;
                }

                object[] parameters = new object[pInfos.Length];
                for (int i = 0; i < pInfos.Length; ++i)
                {
                    parameters[i] = Resolve(pInfos[i].ParameterType);
                }
                return parameters;
            }

            return null;
        }
    }
}
